import collections
with open('day06.txt', 'rt') as file:
    data = file.read().split(',')
newData=[]
for i in data:
    newData.append(int(i))
for i in range(18):
    newFishes = []
    if newData.count(0) > 0:
        for j in range(newData.count(0)):
            newFishes.append(8)
    for k in range(len(newData)):
        if newData[k] == 0:
            newData[k] = 6
        else:
            newData[k] = newData[k]-1
    if len(newFishes) > 0:
        newData.extend(newFishes)
        x = collections.Counter(newData)
print(type(x))
print('part01: ', len(newData))
