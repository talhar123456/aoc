with open('day07.txt', 'rt') as file:
    data = file.read().split(',')
data = [int(x) for x in data]
data.sort()
max = data[-1]
cost = []
for i in range(max+1):
    # print(i)
    fuel = []
    for pos in data:
        fuel.append(abs(pos - i))
    cost.append(sum(fuel))
print('part 01: ', min(cost))
# ****************************************
max = data[-1]
cost = []
for i in range(max+1):
    fuelCost = 0
    for pos in data:
        for j in range(abs(pos - i)+1):
            fuelCost += j
    cost.append(fuelCost)
print('part 02: ', min(cost))


